import React, { Component } from 'react';
import Menu from '../components/Menu.js'
import {TextField, SelectField, MenuItem, Snackbar, RaisedButton} from 'material-ui';
import { withRouter } from 'react-router-dom'
import api from '../api'


class Config extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            ip: "",
            port: 8081,
            open: false,
            statusMsg: "",
            dbtype: ""
        };
    }

    componentDidMount(){
        api.check(isAuth => {
            if(!isAuth){
                this.props.history.replace("/login")
            }
        })
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
        });
    };

    render() {
        const exStyle = {
            fontSize: 11,
            lineHeight: "2px",
            color: "#4a4b4c"
        }
        const buttonStyle = {
            marginLeft: 50
        }
        return (
            <div>
                <Menu />
                <div style={{marginLeft: 100, marginTop: 50}}>
                    <p> Name </p>
                    <TextField
                        label="dbname"
                        hintText="database name"
                        value={this.state.name}
                        onChange={(_,v) => this.setState({name: v}) }
                    />
                    <p> Type of Database </p>
                    <SelectField
                        floatingLabelText="Choose your database type"
                        value={this.state.dbtype}
                        onChange={(e,i,v) => this.setState({dbtype: v})}
                    >
                        <MenuItem value={"Redis"} primaryText="Redis" />
                        <MenuItem value={"Mongodb"} primaryText="Mongodb" />
                    </SelectField>
                    <p> Client IP Address </p>
                    <p style={exStyle}> i.e. 192.168.16.1 </p>
                    <TextField
                        hintText="ip address"
                        value={this.state.ip}
                        onChange={(_,v) => this.setState({ip: v})}
                    />
                    <p> Client Port </p>
                    <p style={exStyle}> i.e. 8080/80/27017 etc. </p>  
                    <TextField
                        hintText="port"
                        value={this.state.port}
                        onChange={(_,v) => this.setState({port: v})}
                    />
                    <br></br>
                    <RaisedButton label="Add" primary={true} style={buttonStyle}
                    onClick={() => {
                        api.addDb(this.state.name, this.state.dbtype, this.state.ip, this.state.port, (isAdded) => {
                            if(isAdded){
                                this.setState({name: "",
                                    dbtype: "",
                                    ip: "",
                                    port: 8080, open: true,
                                statusMsg: "You have successfully added the database client"})

                            }else{
                                this.setState({statusMsg: "You have fail to add the database client", open: true})
                            }
                        })
                    }}
                    />
                    <RaisedButton 
                    label="Finish" 
                    primary={true} 
                    style={buttonStyle}
                    onClick={() => {
                        api.addDb(this.state.name, this.state.dbtype, this.state.ip, this.state.port, (isAdded) => {
                            if(isAdded){
                                this.props.history.push("/")
                            }else{
                                this.setState({statusMsg: "You have fail to add the database client", open: true})
                            }
                        })
                    }}/>
                    <Snackbar
                        open={this.state.open}
                        message={this.state.statusMsg}
                        autoHideDuration={4000}
                        onRequestClose={this.handleRequestClose}
                        bodyStyle={{backgroundColor: "#000000", color: "#FFFFFF"}}
                    />
                </div>
            </div>

        )
    }
}

export default withRouter(Config);