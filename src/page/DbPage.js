import React, { Component } from 'react';
import Menu from '../components/Menu.js'
import {Card, CardTitle, CardText, FlatButton} from 'material-ui';
import {LineChart, Line, XAxis, YAxis, CartesianGrid} from 'recharts'
import api from '../api'


class DbPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            name: "",
            type: "",
            ip: "",
            port: 0,
            status: "",
            averageCpu: 0,
            averageRPS: 0,
            cpuUsage : [],
            RPS: [],

        }
    }

    getData(){
        api.getDbInfoById(this.props.match.params.dbname, (db) => {
            this.setState({
                id: db.databaseId,
                status: db.status,
                averageCpu: db.averageCpuUsage,
                averageRPS: db.averageRequestPerSecond,
                cpuUsage : db.cpuUsage,
                RPS: db.requestPerSecond
            })
        })

        api.getDbById(this.props.match.params.dbname, (db) => {
            this.setState({
                name: db.name,
                type: db.type,
                ip: db.ip,
                port: db.port
            })
        })

    }

    componentDidMount(){
        api.check(isAuth => {
            if(!isAuth){
                this.props.history.replace("/login")
            }
        })

        this.getData()

    }


    render() {
        const style = {
            topCard: {
                marginLeft: 50,
                marginRight: 50,
                marginTop: 50
            }
        }
        return (
            <div>
                <Menu/>
                <Card style={style.topCard}>
                    <CardTitle title="Overall Health" subtitle={"Name: " + this.state.name}/>
                    <CardText>
                        <p> Type: {this.state.type} </p>
                        <p> Health: {this.state.status}</p>
                        <p> Average Cpu Usage: {this.state.averageCpu} </p>
                        <p> Average Request per second: {this.state.averageRPS}</p>

                        <FlatButton label="Refresh" onClick={() => this.getData()}/>
                    </CardText>
                </Card>

                <h2 style={{textAlign: "center"}}>Cpu Usage Overtime </h2>

                <LineChart width={800} height={300} data={this.state.cpuUsage}>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
                    <Line type="monotone" dataKey="cpuUsage" stroke="#8884d8" />
                </LineChart>

                <h2 style={{textAlign: "center"}}>Request per second Usage Overtime </h2>
                <LineChart width={800} height={300} data={this.state.RPS}>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
                    <Line type="monotone" dataKey="requestPerSecond" stroke="#8884d8" />
                </LineChart>


            </div>
            
        )
    }
}

export default DbPage;