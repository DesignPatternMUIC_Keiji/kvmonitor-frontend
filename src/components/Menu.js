import React, { Component } from 'react';
import {AppBar, FlatButton, IconButton} from 'material-ui';
import { withRouter } from 'react-router-dom'
import api from '../api'


class Menu extends Component {

    

    render() {
        let pageName = "Dashboard"

        if (this.props.location.pathname === "/config"){
            pageName = "Configuration"
        }else if(this.props.location.pathname.includes("/db")) {
            pageName = "Database Status"
        }

        return(
            <AppBar
                title={
                    <div> 
                       <p style={{ lineHeight: "0px" }}> KV-Monitor </p>
                       <p style={{fontSize: "18px", lineHeight: "0px", paddingRight: "30px"}}> {pageName} </p>
                    </div>
                }
                iconElementRight={
                pageName === "Configuration" ? (
                    <div style={{ display: "inline-block" }}>
                        <FlatButton label="Logout" onClick={() => api.logout(status => {
                            if(status){
                                this.props.history.replace("/login")
                            }
                        })} />
                        <FlatButton label="Back" onClick={() => this.props.history.push("/")}  />
                    </div>
            ) : (
                        <div style={{ display: "inline-block" }}>
                            <FlatButton label="Logout" onClick={() => api.logout(status => {
                                if(status){
                                    this.props.history.replace("/login")
                                }
                            })} />
                            <FlatButton onClick={() => this.props.history.push("/config")} label="Config" />
                                {pageName !== "Dashboard" ? <FlatButton label="Back" onClick={() => this.props.history.push("/")} />
                                : (null)
                            }
                        </div>
                    )
                
                }
                iconElementLeft={<IconButton>></IconButton>}
            />
        )
    }
}

export default withRouter(Menu);