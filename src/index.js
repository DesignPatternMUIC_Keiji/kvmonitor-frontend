import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './page/App';
import Config from './page/Config'
import DbPage from './page/DbPage'
import Login from './page/Login'
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import * as Colors from 'material-ui/styles/colors';
import api from './api'

const muiTheme = getMuiTheme({
    palette: {
        textColor: Colors.darkBlack,
        primary1Color: Colors.grey300,
        primary2Color: Colors.indigo700,
        accent1Color: Colors.faintBlack,
        pickerHeaderColor: Colors.darkBlack,
        alternateTextColor: Colors.darkBlack
    },
    appBar: {
        height: 75
    }
});


const MyRouter = () => (
    <Router>
        <Switch>
            <Route path="/login" component={Login}/>
            <Route exact path="/" component={App}/>
            <Route path="/config" component={Config} />
            <Route path="/db/:dbname" component={DbPage}/>
        </Switch>
    </Router>
)

ReactDOM.render(
    <MuiThemeProvider muiTheme={muiTheme}>
     <MyRouter />
     </MuiThemeProvider>
   , document.getElementById('root')
);
registerServiceWorker();
