import React, { Component } from 'react';
import { TextField, RaisedButton } from 'material-ui';
import { withRouter } from 'react-router-dom'
import api from '../api'

class Login extends Component {


    constructor(props) {
        super(props)
        this.state = {"username": "", "password": ""}
    }

    componentDidMount() {
        api.check(isAuth => {
            if (isAuth){
                this.props.history.replace("/")
            }
        })
    }

    render () {
        const style = {"textAlign": "center"}
        return (
            <div style={style}>
                <h1> Login to KVMonitor </h1>
                <p> username </p>
                <TextField name="username" value={this.state.username} onChange={(_,v) => {this.setState({"username": v})}}/>
                <p> password </p>
                <TextField name="password" type="password" onChange={(_,v) => {this.setState({"password": v})}}/>
                <p></p>
                <RaisedButton label="Login" primary={true}
                              onClick={(e) => {api.login(this.state.username, this.state.password, (isOk) => {
                                  console.log("isOk " + isOk)
                    if (isOk) {
                        this.props.history.replace("/")
                    }else{
                        this.setState({"username": "", "password": ""})
                        console.log("fail to login")
                    }
                })}} />
            </div>
        )
    } 
 }

export default withRouter(Login);
