import React, { Component } from 'react';
import Menu from '../components/Menu.js'
import DbCard from '../components/DbCard.js'
import {FloatingActionButton} from 'material-ui'; 
import ContentAdd from 'material-ui/svg-icons/content/add'
import { Row, Col } from 'react-flexbox-grid';
import { withRouter } from 'react-router-dom'
import api from '../api'

class App extends Component {


    constructor(props) {
        super(props)
        this.state = {databases: []}
    }

    componentDidMount(){
    api.check(isAuth => {
      if(!isAuth){
          this.props.history.replace("/login")
      }
    })

    api.getAllDb(db => {
      this.setState({databases: db})
    })
  }

  render() {

    const size = 4

    return (
      <div>
        <Menu title="Dashboard"/>
        <Row around="xs" style={{ marginTop: 50, marginBottom: 50 }}>
            {this.state.databases.map(db =>
              <Col xs={size}><DbCard dbname={db.name} dbtype={db.type} health={db.status}
                                     cpu={db.averageCpuUsage} request={db.averageRequestPerSecond}
                                      ip={db.ip} port={db.port} id={db.id}/></Col>
            )}
            <Col xs={size}>
              <FloatingActionButton onClick={() => this.props.history.push("/config")}>
                <ContentAdd />
            </FloatingActionButton>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(App);
