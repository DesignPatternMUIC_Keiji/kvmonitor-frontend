import React, { Component } from 'react';
import {FlatButton, Card, CardActions, CardText, CardHeader} from 'material-ui';
import {withRouter} from 'react-router-dom'
import api from '../api'


class DbCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            // style = {{ width: "300px", height: "250px" }}
            <Card style = {{textAlign: "left"}}>
                <CardHeader
                    title={this.props.dbname}
                    subtitle={this.props.dbtype}
                />
                <CardText children={
                    <ul style={{listStyle: "none", textAlign: "left"}}>
                        <li> Ip: {this.props.ip} port: {this.props.port} </li>
                        <li> port: {this.props.port}</li>
                        <li> Health: {this.props.health} </li>
                        <li> Average CPU: {this.props.cpu}% </li>
                        <li> Average RPS: {this.props.request} </li>
                    </ul>
                } />
                <CardActions>
                    <FlatButton label="delete" onClick={() => {
                      api.deleteDbById(this.props.id, (res) => {
                        if(res){
                          window.location.reload()
                        }
                      })
                    }} />
                    <FlatButton onClick={() => this.props.history.push("/db/" + this.props.id)} label="more" />
                </CardActions>
            </Card>
        ) 
    }
}

export default withRouter(DbCard);
