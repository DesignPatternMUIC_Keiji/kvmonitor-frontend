import axios from 'axios'
import FormData from 'form-data'

const instance = axios.create({
    baseURL: 'http://localhost:8080',
    withCredentials: true
})

const jsonHeader = {'Content-Type': 'application/json'}

export default {
    login(username, password, cb) {
        const form = new FormData()
        form.append("username", username)
        form.append("password", password)
        instance.post('/login', form,
            {headers: {
                'Content-Type': 'multipart/form-data'
            }})
            .then(res => {
            if (res.status === 200) {
                cb(true)
            } else {
                cb(false)
            }
        }).catch(ex => {
            console.log("ex" + ex)
            return cb(false)
        })
    },
    check(cb) {
        instance.get('user/check')
            .then(res => {
                if(res.status === 200){
                    cb(true)
                }else{
                    cb(false)
                }
            }).catch(ex => {
                console.log(ex)
                cb(false)
            })
    },
    logout(cb) {
        instance.get('/logout')
            .then(res => {
                if(res.status === 200){
                    cb(true)
                }else{
                    cb(false)
                }
                console.log("successfully logout")
            })
            .catch(ex => {
                console.log("fail to logout")
                console.log(ex)
            })
    },
    getAllDb(cb) {
        instance.get('/database/all')
            .then(res => {
                if(res.status === 200){
                    cb(res.data)
                }else{
                    console.log("fail to fetch db")
                    cb([])
                }
            })
            .catch(ex => {
                console.log(ex)
            })
    },
    addDb(name, type, ip, port, cb) {
        instance.post('/database/add',{
            "name": name,
            "type": type,
            "ip": ip,
            "port": port
        })
            .then(res => {
                if(res.status === 200){
                    cb(true)
                }else{
                    cb(false)
                }
            })
            .catch(ex => {
                console.log(ex)
            })
    },
    getDbInfoById(id, cb){
        instance.get('/database_info/info?databaseId=' + id)
            .then(res => {
                if(res.status === 200){
                    cb(res.data)
                }else{
                    cb({})
                }
            })
            .catch(ex => {
                console.log(ex)
                cb({})
            })
    },
    getDbById(id, cb){
        instance.get('/database/info?id=' + id)
            .then(res => {
                if(res.status === 200){
                    cb(res.data)
                }else{
                    cb({})
                }
            }).catch(ex => {
                cb({})

            })
    },
    deleteDbById(id, cb){
      instance.get('/database/delete?id=' + id)
        .then(res => {
          if(res.status === 200){
            cb(true)
          }else{
            cb(false)
          }
        }).catch(ex => {
          console.log(ex)
        })
    }
}
